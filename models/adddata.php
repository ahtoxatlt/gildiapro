<?php
include '../migrations/connect_task.php';
$task = $_POST['task'];
$author = $_POST['author'];
$status = $_POST['status'];
$sql = 'INSERT INTO task(task, author, status) VALUES (:task, :author, :status)';
$query = $pdo->prepare($sql);
$query->execute(['task'=>$task, 'author' => $author, 'status' => $status]);

header('location:http://localhost/gildiapro/views/task_book.php');
