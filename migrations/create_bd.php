<?php
$conn = 'mysql:host=localhost; dbname=task_book';
$pdo = new PDO($conn,'mysql', 'mysql');
$data = $pdo->prepare('CREATE TABLE task (
    id int not null unique auto_increment,
    task varchar (255) not null,
    author varchar (255) not null,
    status varchar (255) not null 
)');
$data->execute();
